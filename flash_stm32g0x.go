/*
 * Copyright (c) Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

package flash_stm32g0x

import (
	"log"
	"os/exec"
	"strconv"
	"strings"
)

var (
	ocd_if     string
	ocd_target string
	Logger     log.Logger = *log.Default()
)

func init() {
	Logger.SetPrefix("ServiceOpenOCD: ")
	Logger.Println("Starting")
}

// Configure the flash_stm32g0x package
//
//	Parameter:
//	  - ocd_interface: Path of the config file of the used openocd interface
//	  - target: Path of the config file of the used opeocd target
func Configure(ocd_interface string, target string) {
	ocd_if = ocd_interface
	ocd_target = target
}

// Reset target and keep it halted after the reset
func ResetHalt() {
	cmd := "openocd"
	cmd = cmd + " -f " + ocd_if
	cmd = cmd + " -f " + ocd_target
	cmd = cmd + " -c 'init'"
	cmd = cmd + " -c 'reset halt'"
	cmd = cmd + " -c 'exit'"
	c := exec.Command("sh", "-c", cmd)
	stdout, _ := c.CombinedOutput()
	Logger.Print(string(stdout[:]))
}

// Reset target and set cpu into running mode
func ResetRun() {
	cmd := "openocd"
	cmd = cmd + " -f " + ocd_if
	cmd = cmd + " -f " + ocd_target
	cmd = cmd + " -c 'init'"
	cmd = cmd + " -c 'reset run'"
	cmd = cmd + " -c 'exit'"
	c := exec.Command("sh", "-c", cmd)
	stdout, _ := c.CombinedOutput()
	Logger.Print(string(stdout[:]))
}

// Unlock the flash memory
func UnlockFlashMem() error {
	cmd := "openocd"
	cmd = cmd + " -f " + ocd_if
	cmd = cmd + " -f " + ocd_target
	cmd = cmd + " -c 'init'"
	cmd = cmd + " -c 'mww 0x40022008 0x45670123'"
	cmd = cmd + " -c 'mww 0x40022008 0xCDEF89AB'"
	cmd = cmd + " -c 'mdw 0x40022014'"
	cmd = cmd + " -c 'exit'"
	c := exec.Command("sh", "-c", cmd)
	stdout, err := c.CombinedOutput()
	if err != nil {
		Logger.Print(err)
		return err
	}

	res := strings.Split(string(stdout[:]), "\n")
	opt_line := res[len(res)-3]
	opt_res := strings.Split(opt_line, " ")
	opt_bytes, err := strconv.ParseInt(opt_res[len(opt_res)-2], 16, 64)
	if err != nil {
		Logger.Print(err)
		return err
	}
	if opt_bytes > 0x40000000 {
		Logger.Print("Unlocking flash mem not successful")
		return err
	}
	return nil
}

// Unlock the flash option bytes
func UnlockFlashOpts() error {
	cmd := "openocd"
	cmd = cmd + " -f " + ocd_if
	cmd = cmd + " -f " + ocd_target
	cmd = cmd + " -c 'init'"
	cmd = cmd + " -c 'mww 0x4002200C 0x08192A3B'"
	cmd = cmd + " -c 'mww 0x4002200C 0x4C5D6E7F'"
	cmd = cmd + " -c 'mdw 0x40022014'"
	cmd = cmd + " -c 'exit'"
	c := exec.Command("sh", "-c", cmd)
	stdout, err := c.CombinedOutput()
	if err != nil {
		Logger.Print(err)
		return err
	}
	res := strings.Split(string(stdout[:]), "\n")
	opt_line := res[len(res)-3]
	opt_res := strings.Split(opt_line, " ")
	opt_bytes, err := strconv.ParseInt(opt_res[len(opt_res)-2], 16, 64)
	if err != nil {
		Logger.Print(err)
		return err
	}
	if opt_bytes > 0x3F0000000 {
		Logger.Print("Unlocking flash mem not successful")
		return err
	}
	return nil
}

// Write the option bytes of the target
//
// Parameter:
//   - option_bytes: int - The option bytes to be written
func WriteOptionBytes(option_bytes int64) error {
	cmd := "openocd"
	cmd = cmd + " -f " + ocd_if
	cmd = cmd + " -f " + ocd_target
	cmd = cmd + " -c 'init'"
	cmd = cmd + " -c 'mww 0x40022020 0x" + strconv.FormatInt(option_bytes, 16) + "'"
	cmd = cmd + " -c 'exit'"
	c := exec.Command("sh", "-c", cmd)
	stdout, err := c.CombinedOutput()
	if err != nil {
		Logger.Print(string(stdout[:]))
		Logger.Print(err)
		return err
	}
	return nil
}

// Read the option bytes of the target
//
// Returns the read option bytes
func ReadOptionBytes() (int64, error) {
	cmd := "openocd"
	cmd = cmd + " -f " + ocd_if
	cmd = cmd + " -f " + ocd_target
	cmd = cmd + " -c 'init'"
	cmd = cmd + " -c 'mdw 0x40022020'"
	cmd = cmd + " -c 'exit'"
	c := exec.Command("sh", "-c", cmd)
	stdout, err := c.CombinedOutput()
	if err != nil {
		Logger.Print(string(stdout[:]))
		Logger.Print(err)
		return 0, err
	}
	res := strings.Split(string(stdout[:]), "\n")
	opt_line := res[len(res)-3]
	opt_res := strings.Split(opt_line, " ")
	opt_bytes, err := strconv.ParseInt(opt_res[len(opt_res)-2], 16, 64)
	if err != nil {
		Logger.Print(err)
		return 0, err
	}
	return opt_bytes, nil
}

// Flash an image file into the flash memory of the target
//
// Parameter:
//   - path: string - The file path of the image file to be flashed
func FlashImage(path string) error {
	cmd := "openocd"
	cmd = cmd + " -f " + ocd_if
	cmd = cmd + " -f " + ocd_target
	cmd = cmd + " -c 'init'"
	cmd = cmd + " -c 'stm32g0x mass_erase 0'"
	cmd = cmd + " -c 'flash write_image " + path + "'"
	cmd = cmd + " -c 'exit'"
	c := exec.Command("sh", "-c", cmd)
	stdout, err := c.CombinedOutput()
	if err != nil {
		Logger.Print(string(stdout[:]))
		Logger.Print(err)
		return err
	}
	return nil
}
