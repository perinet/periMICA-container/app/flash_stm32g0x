# flash stm32g0x application changelog

# Upstream

# 0.0.1

* Flash firmware image
* Read option bytes
* Write option bytes
* Reset target
